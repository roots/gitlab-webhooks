This repo contains webhooks for our gitlab.

# gitlabWebhookDeploy.py
This script is intended to be added as push hook in Gitlab. When the
script is called it checks if the push is relevant for the checkout and
updates the local checkout if necessary. Deployment can be done using
uWSGI which allows the script to run as a different user then the www-data
user.

Currently it is pretty hardcoded, but should become more flexible with
some kind of configuration.

## TODO
- [ ] remove hardcoded config
- [ ] possibility to make some folders group writable
- [x] store .git outside of webpath

# ircNotify.py
Simple IRC notifier script which uses another custom script for relaying
the messages to IRC. All it does is does is calling different hooks based
on the event type, which send messages to the IRC server

## TODO
- [ ] allow to specify the channel via url parameter (?channel=#gaf-alarm)
- [ ] allow to restrict to special categories via url parameter
