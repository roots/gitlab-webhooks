#!/usr/bin/env python

import json
import os.path
import os
import re
from subprocess import call

def getConfig(name):
    config = {}
    config['git_dir'] = "/home/www-hook/repos/"
    config['git_branch'] = "web"
    config['git_url_regexp'] = r'git@git\.fs\.lmu\.de:'

    if name == 'unikult':
        config['type'] = 'group'
        config['git_work_tree'] = "/srv/http/de.uni-kult.tools/htdocs"
        config['git_dir'] = "/srv/http/de.uni-kult.tools/repositories"
        config['git_branch'] = "web"
        config['git_url_regexp'] = r'git@git\.fs\.lmu\.de:unikult/'
    if name == 'gaf':
        config['type'] = 'group'
        config['git_work_tree'] = "/srv/http/de.lmu.fs.gaf/htdocs"
        config['git_dir'] = "/srv/http/de.lmu.fs.gaf/repositories"
        config['git_branch'] = "web"
        config['git_url_regexp'] = r'git@git\.fs\.lmu\.de:GAF/'
    if name == 'webhooks':
        config['type'] = 'single'
        config['git_work_tree'] = "/srv/http/de.lmu.fs.www/cgi-bin/webhooks"
        config['git_dir'] = "/home/www-hook/repos/webhooks"
        config['git_branch'] = "master"
        config['git_url_regexp'] = r'git@git\.fs\.lmu\.de:roots/gitlab-webhooks\.git'

    return config

def application(environ, start_response):
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    request_body = environ['wsgi.input'].read(request_body_size)

    if not 'WEBHOOK_CONFIG' in environ:
        wrongUsage(environ, start_response,
                "No WEBHOOK_CONFIG given.")
    config = getConfig(environ['WEBHOOK_CONFIG'])

    # this code assumes gitlab formated json input
    try:
        j = json.loads(request_body)

        if not re.search(config['git_url_regexp'],
                j['repository']['git_ssh_url']):
            return wrongUsage(environ, start_response,
                    "git ssh url invalid\n")

        if j['object_kind'] != 'push':
            return wrongUsage(environ, start_response)

        if j['ref'] != 'refs/heads/%s'%config['git_branch']:
            return ok(environ, start_response,
                    "Noting to do for branch %s\n"%config['git_branch'])

        name = j['repository']['name']
        url = j['repository']['git_ssh_url']
    except (ValueError):
        return wrongUsage(environ, start_response)

    if config['type'] == 'single':
        repodir = config['git_dir']
        workdir = config['git_work_tree']
    elif config['type'] == 'group':
        repodir = os.path.join(config['git_dir'], name)
        workdir = os.path.join(config['git_work_tree'], name)
    else:
        return wrongUsage(environ, start_response,
                'invalid config type\n')

    if os.path.isdir(workdir):
        call(["git", "--git-dir", repodir, "--work-tree", workdir, "fetch"])
        call(["git", "--git-dir", repodir, "--work-tree", workdir,
            "checkout", "--force", "origin/%s"%config['git_branch']])
    else:
        call(["git", "clone", "--branch", config['git_branch'],
            "--single-branch", "--separate-git-dir", repodir, url, workdir])

    response_body = [
        '%s: %s' % (key, value) for key, value in
        sorted(environ.items())
                        ]
    response_body = '\n'.join(response_body)

    status = '200 OK'
    response_headers = [
            ('Content-Type', 'text/plain'),
            ('Content-Length', str(len(response_body)))
        ]
    start_response(status, response_headers)

    return [response_body]

def ok(env, start_response, message = ''):
    response_body = 'OK\n' + message
    status = '200 OK'
    response_headers = [
            ('Content-Type', 'text/plain'),
            ('Content-Length', str(len(response_body))),
        ]

    start_response(status, response_headers)

    return [response_body]

def wrongUsage(env, start_response, message = ''):
    response_body = 'Permission Denied\n' + message
    status = '403 Permission Denied'
    response_headers = [
            ('Content-Type', 'text/plain'),
            ('Content-Length', str(len(response_body))),
        ]

    start_response(status, response_headers)

    return [response_body]
