#!/usr/bin/env python

import json
import requests

def niceTime(t):
    t = int(t)
    if t >= 3600:
        return str(t / 3600) + 'h' + str(t / 60) + 'm' + str(t%60) + 's'
    elif t >= 60:
        return str(t / 60) + 'm' + str(t%60) + 's'
    else:
        return str(t) + 's'

def addExtraArgs(j):
    j.update(j['repository'])
    if j['object_kind'] == 'build':
        j['branch'] = j['ref']
    else:
        j['branch'] = j['ref'].split('/')[2]
    j['group'] = j['homepage'].split('/')[3]
    j['repo'] = j['homepage'].split('/')[4]
    return j

def addExtraCommitArgs(j):
    splitMessage = j['message'].split('\n', 1)
    j['short_message'] = splitMessage[0].strip()
    j['long_message'] = splitMessage[1].strip() if len(splitMessage) > 1 else ''
    j['short_url'] = j['url'][:-32]
    return j

def pushHook(j, environ, session):
    j = addExtraArgs(j)
    sendTpl('push_summary', j, session)
    for commit in j['commits']:
        commit = addExtraCommitArgs(commit)
        sendTpl('push_commit', commit, session)

    return "Executed push hook\n"

def buildHook(j, environ, session):
    j = addExtraArgs(j)
    splitMessage = j['commit']['message'].split('\n', 1)
    j['short_message'] = splitMessage[0].strip()
    j['long_message'] = splitMessage[1].strip() if len(splitMessage) > 1 else ''
    j['nice_build_duration'] = niceTime(j['build_duration'])
    j['build_url'] = j['homepage'] + '/builds/' + str(j['build_id'])
    j['short_sha'] = j['sha'][:-32]
    if j['build_stage'] != 'build':
        return "Nothing to do for non build stage\n"

    if j['build_status'] == 'success':
        sendTpl('build_success', j, session)
    elif j['build_status'] == 'failed':
        sendTpl('build_failure', j, session)
    elif j['build_status'] == 'running':
        sendTpl('build_running', j, session)

    else:
        # to find failure state
        return testHook(j, environ, session)

    return "Executed build hook\n"

def noHook(j, environ, session):
    return "no hook defined\n"

def testHook(j, environ, session):
    with open('/etc/webhooks/irckey', 'r') as f:
        key = f.read().strip()
    text = str(j)
    while len(text) > 400:
        send(text[:400], key, session, chans = ['#gaf-alarm']) 
        text = text[400:]
    send(text, key, session, chans = ['#gaf-alarm']) 
    return "tested"

hooks = {'push': pushHook,
        'build': buildHook,
        }

templates = {
        'push_summary': u"[\x0306{group}\x0F/\x0306\x02{repo}\x0F] " +
            "{user_name} pushed \x02{total_commits_count}\x0F commits " +
            "to branch \x0306{branch}\x0F",
        'push_commit': u" -> {short_message} \x0314({short_url})\x0F",
        'build_success': u"[\x0306{group}\x0F/\x0306\x02{repo}\x0F] " +
            "build \x0309\x02success\x0F for \x0306{short_sha}\x0F " +
            "after {nice_build_duration}\n" +
            "\x0314 -> {build_url}\x0F",
        'build_failure': u"[\x0306{group}\x0F/\x0306\x02{repo}\x0F] " +
            "build \x0304\x02failure\x0F for \x0306{short_sha}\x0F " +
            "after {nice_build_duration}\n" +
            "\x0314 -> {build_url}\x0F",
        'build_running': u"[\x0306{group}\x0F/\x0306\x02{repo}\x0F] " +
            "started build for \x0306{short_sha}\x0F ({short_message})\n" +
            "\x0314 -> {build_url}\x0F",
    }

def application(environ, start_response):
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
        request_body = environ['wsgi.input'].read(request_body_size)
        j = json.loads(request_body)
    except ValueError as e:
        return wrongUsage(environ, start_response, str(e))

    with requests.Session() as s:
        hook = hooks.get(j['object_kind'], testHook)
        response_body = hook(j, environ, s)
    if response_body == None:
        response_body = ''
    status = '200 OK'

    response_headers = [
            ('Content-Type', 'text/plain'),
            ('Content-Length', str(len(response_body))),
        ]

    start_response(status, response_headers)

    return [response_body]

def sendTpl(tpl, vals, session):
    with open('/etc/webhooks/irckey', 'r') as f:
        key = f.read().strip()
    text = templates[tpl].format(**vals)
    for line in text.split('\n'):
        send(line, key, session, chans = ['#gaf-alarm'])

## @param msg A single line of text, which should be send to some IRC channels.
##     Only the first line of a multi-line message will be send. For IRC
##     formatting see https://github.com/myano/jenni/wiki/IRC-String-Formatting
## @param key API-Key. Ask skruppy.
## @param chans If None (the default), the message will be send to all default
##     channels defined by the web hook. Otherwise to the list of channels or
##     a single channel, if defined as a string.
## @param url API URL
def send(msg, key, session, chans = None, url = 'http://xmpp:8008'):
    params = {'msg': msg, 'key': key}

    if isinstance(chans, list):
        params['chans'] = ','.join(chans)
    elif isinstance(chans, str):
        params['chans'] = chans

    return session.post(url, params=params).raise_for_status()

def wrongUsage(env, start_response, message = ''):
    response_body = 'Permission Denied\n' + message
    status = '403 Permission Denied'
    response_headers = [
            ('Content-Type', 'text/plain'),
            ('Content-Length', str(len(response_body))),
        ]

    start_response(status, response_headers)

    return [response_body]
